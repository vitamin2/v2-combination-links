# v2-combination-links plugin for Kirby 3

> Just an plugin for combination links (works with vitamin2/v2-spam-free-mail)

****

## Installation

### Download

Download and copy this repository to `/site/plugins/v2-combination-links`.

### Composer

```
composer require vitamin2/v2-combination-links
```

## Setup

1. Install Plugin

2. Use Field in your blueprint

```yml
  combinationLinks:
    extends: global/extend/combination-links
```

3. Use in your template

```php
    snippet("organisms/combination-links", [
      "combinationLinks" => $data->combinationLinks(),
    ]);
```

## Modify

### Config
In the config file you can set the default classes for the button

```yaml
'vitamin2.v2-combination-links.buttonClasses' => 'custom-button-class',
```

### Snippets

You can modify the snippets `organisms/combination-links` and `atoms/button` to your needs.

Just create your own combination-links snippet in your `snippets/organisms` folder.
Original snippet:

```
<div class="combination-links">
  <?php foreach ($combinationLinks()->toStructure() as $subitem): ?>
    <?php getCombinationLink($site, $subitem); ?>
  <?php endforeach ?>
</div>
```
Just create your own button snippet in your `snippets/atoms` folder.
Original snippet:

```
<a href="<?= $linkTo ?>" class="btn <?= $btnClass ?>" <?= $newTab ? 'target="_blank" rel="noopener noreferrer"' : "" ?> title="<?= isset($linkTitle) ? $linkTitle : "Button" ?>">
    <?= $btnText ?>
    <?php snippet("atoms/icons/icon-" . $iconName); ?>
</a>
```

## License

MIT
