<a href="<?= $linkTo ?>" class="<?= $btnClass ?>" <?= $newTab
    ? 'target="_blank" rel="noopener noreferrer"'
    : "" ?> title="<?= isset($linkTitle) ? $linkTitle : "Button" ?>">
    <?= $btnText ?>
    <?php snippet("atoms/icons/icon-" . $iconName); ?>
</a>
