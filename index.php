<?php

Kirby::plugin("vitamin2/v2-combination-links", [
    "snippets" => [
        "organisms/combination-links" => __DIR__ . "/snippets/combination-links.php",
        "atoms/button" => __DIR__ . "/snippets/button.php",
    ],
    "blueprints" => [
        "global/extend/combination-links" => __DIR__ . "/blueprints/combination-links.yml",
    ],
    "options" => [
        "buttonClasses" => "btn btn-center btn-icon btn-icon-right btn-bordered btn-rounded",
    ],
]);

// combination link calculator
function getCombinationLink($site, $element)
{
    $typeValue = $element->category()->value();
    $btnClasses = option("vitamin2.v2-combination-links.buttonClasses");
    if ($typeValue === "externalPage") {
        if ($element->optionA()->isNotEmpty()) {
            snippet("atoms/button", [
                "linkTo" => $element->optionA()->toUrl(),
                "linkTitle" => $element
                    ->linkTitle()
                    ->or($element->optionText())
                    ->or($element->optionA()->toUrl()),
                "btnText" => $element->optionText()->or($element->optionA()->toUrl()),
                "btnClass" => $btnClasses,
                "iconName" => "arrow-right",
                "newTab" => true,
            ]);
        }
    } elseif ($typeValue === "internalPage") {
        if ($element->optionB()->isNotEmpty() && $site->find($element->optionB()->toPage())) {
            snippet("atoms/button", [
                "linkTo" => $element
                    ->optionB()
                    ->toPage()
                    ->url(),
                "linkTitle" => $element->linkTitle()->or(
                    $element->optionText()->or(
                        $element
                            ->optionB()
                            ->toPage()
                            ->title()
                    )
                ),
                "btnText" => $element->optionText()->or(
                    $element
                        ->optionB()
                        ->toPage()
                        ->title()
                ),
                "btnClass" => $btnClasses,
                "iconName" => "arrow-right",
                "newTab" => false,
            ]);
        }
    } elseif ($typeValue === "fileType") {
        if ($element->optionC()->isNotEmpty() && $element->optionC()->toFile()) {
            snippet("atoms/button", [
                "linkTo" => $element
                    ->optionC()
                    ->toFile()
                    ->url(),
                "linkTitle" => $element->linkTitle()->or(
                    $element->optionText()->or(
                        $element
                            ->optionC()
                            ->toFile()
                            ->filename()
                    )
                ),
                "btnText" => $element->optionText()->or(
                    $element
                        ->optionC()
                        ->toFile()
                        ->filename()
                ),
                "btnClass" => $btnClasses,
                "iconName" => "download",
                "newTab" => true,
            ]);
        }
    } elseif ($typeValue === "emailType") {
        mailHandling($element, $btnClasses);
    } elseif ($typeValue === "phoneType") {
        if ($element->optionE()->isNotEmpty()) {
            snippet("atoms/button", [
                "linkTo" => "tel:" . str_replace(" ", "", $element->optionE()),
                "linkTitle" => $element->linkTitle()->or($element->optionText()->or($element->optionE())),
                "btnText" => $element->optionText()->or($element->optionE()),
                "btnClass" => $btnClasses,
                "iconName" => "phone",
                "newTab" => false,
            ]);
        }
    }
}

function mailHandling($element, $btnClasses)
{
    if ($element->optionD()->isNotEmpty()) {
        $kirby = kirby();
        if ($kirby->plugin("vitamin2/v2-spam-free-mail")) {
            snippet("global/protected-mail", [
                "emailAddress" => $element->optionD(),
                "emailText" => $element->optionText()->or($element->optionD()),
                "customAttributes" => [
                    "class" => $btnClasses,
                    "rel" => "noopener noreferrer",
                    "title" => $element->linkTitle()->or($element->optionText()->or($element->optionD())),
                ],
            ]);
        } else {
            snippet("atoms/button", [
                "linkTo" => "mailto:" . $element->optionD(),
                "linkTitle" => $element->linkTitle()->or($element->optionText()->or($element->optionD())),
                "btnText" => $element->optionText()->or($element->optionD()),
                "btnClass" => $btnClasses,
                "iconName" => "email",
                "newTab" => false,
            ]);
        }
    }
}
